package com.example.repositories;

import com.example.models.Collection;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by flesk on 07.12.16.
 */
public interface CollectionRepository extends CrudRepository<Collection, Long> {

    List<Collection> findAll();
    Collection save(Collection collection);
}
