package com.example.repositories;

import com.example.models.CollectionItemId;
import com.example.models.Item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by eivind on 2016-12-09.
 */
public interface ItemRepository extends CrudRepository<Item, CollectionItemId> {

    List<Item> findByIdCollectionId(Long collectionId);

    @Query("SELECT MAX(i.id.itemId) + 1 from Item i where i.id.collectionId = :collectionId")
    Long getNextItemId(@Param("collectionId") Long collectionId);
}
