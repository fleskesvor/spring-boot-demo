package com.example.services;

import com.example.exceptions.EntityNotFoundException;
import com.example.exceptions.EntityNotSavedException;
import com.example.models.Collection;
import com.example.models.CollectionItemId;
import com.example.models.Item;
import com.example.repositories.CollectionRepository;
import com.example.repositories.ItemRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by flesk on 12.12.16.
 */
@Service
@Transactional
public class CollectionServiceImpl implements CollectionService {

    static final Logger logger = LogManager.getLogger(CollectionServiceImpl.class.getName());

    @Autowired
    CollectionRepository collectionRepository;

    @Autowired
    ItemRepository itemRepository;

    @Override
    public List<Collection> getCollections() {
        List<Collection> collectionList = collectionRepository.findAll();
        if (collectionList == null || collectionList.size() == 0)
            throw new EntityNotFoundException("No collections found");
        return collectionList;
    }

    @Override
    public Collection getCollection(Long collectionId) {
        Collection collection = collectionRepository.findOne(collectionId);
        if (collection == null)
            throw new EntityNotFoundException("No collection found for collectionId " + collectionId);
        return collection;
    }

    @Override
    public Collection saveCollection(Collection collection) {
        collectionRepository.save(collection);
        // TODO: This returns the same, cached collection as above, with incorrect
        // TODO: firstItem if changed, since it isn't mapped properly to collection
        return collectionRepository.findOne(collection.getCollectionId());
    }

    @Override
    public List<Item> getItems(Long collectionId) {
        List<Item> itemList = itemRepository.findByIdCollectionId(collectionId);
        if (itemList == null || itemList.size() == 0)
            throw new EntityNotFoundException("No items found for collectionId " + collectionId);
        return itemList;
    }

    @Override
    public Item getItem(Long collectionId, Long itemId) {
        Item item = itemRepository.findOne(new CollectionItemId(collectionId, itemId));
        if (item == null)
            throw new EntityNotFoundException("No item found for collectionId " + collectionId + " + itemId " + itemId);
        return item;
    }

    @Override
    public Item saveItem(Long collectionId, Item item) {
        return save(collectionId, item);
    }

    private Item save(Long collectionId, Item item) {
        // TODO: Better exception handling -- use multi-catch and no generic Exception
        // TODO: See if we can get an Id from DB, eg. with a trigger
        Long itemId = null;

        if (item.getId() == null || item.getId().getItemId() == null) {
            itemId = itemRepository.getNextItemId(collectionId);
            if (itemId == null)
                throw new EntityNotFoundException("No collection found for collectionId " + collectionId);
            item.setId(collectionId, itemId);
        }

        try {
            return itemRepository.save(item);
        } catch (Exception exception) {
            throw new EntityNotSavedException(exception.getMessage());
        }
    }
}
