package com.example.services;

import com.example.models.Collection;
import com.example.models.Item;

import java.util.List;

/**
 * Created by eivind on 2016-12-13.
 */
public interface CollectionService {

    List<Collection> getCollections();
    Collection getCollection(Long collectionId);
    Collection saveCollection(Collection collection);

    List<Item> getItems(Long collectionId);
    Item getItem(Long collectionId, Long itemId);
    Item saveItem(Long collectionId, Item item);
}
