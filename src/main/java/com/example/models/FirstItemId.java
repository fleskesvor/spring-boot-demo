package com.example.models;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by eivind on 2016-12-12.
 */
@Data
@Embeddable
public class FirstItemId implements Serializable {
    @OneToOne
    @JoinColumns({
            @JoinColumn(name="collection_id", referencedColumnName="collection_id", insertable = false, updatable =
                    false),
            @JoinColumn(name="first_item", referencedColumnName="item_id")
    })
    Item firstItem;
}
