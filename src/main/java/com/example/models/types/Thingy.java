package com.example.models.types;

import com.example.models.Item;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by flesk on 07.12.16.
 */
@Data
@Entity
public class Thingy extends Item {
    private String name;
    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "collection_id", referencedColumnName = "collection_id",
                    insertable = false, updatable = false),
            @JoinColumn(name = "next_item", referencedColumnName = "item_id",
                    insertable = false, updatable = false)
    })
    private Item nextItem;
    @Column(name = "first_item")
    @JsonIgnore
    private Long nextItemId;

    public void setNextItem(Item nextItem) {
        // TODO: Propagate any changes to nextItem too?
        if (nextItem == null || nextItem.getId() == null || nextItem.getId().getItemId() == null)
            this.nextItemId = null;
        else
            this.nextItemId = nextItem.getId().getItemId();
    }

    public Thingy() {
        super.setItemType("Thingy");
    }

    @Override
    public String toString() {
        return "Thingy{" +
                "id=" + super.getId() +
                ", name='" + name + '\'' +
                ", nextItem=" + nextItem +
                '}';
    }
}
