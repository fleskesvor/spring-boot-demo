package com.example.models.types;

import com.example.models.Item;
import lombok.Data;

import javax.persistence.Entity;

/**
 * Created by flesk on 07.12.16.
 */
@Data
@Entity
public class Kajigger extends Item {
    private String name;
    private String description;

    public Kajigger() {
        super.setItemType("Kajigger");
    }

    @Override
    public String toString() {
        return "Kajigger{" +
                "id=" + super.getId() +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
