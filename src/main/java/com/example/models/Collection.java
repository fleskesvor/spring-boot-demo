package com.example.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by flesk on 07.12.16.
 */
@Data
@Entity
@Table(name = "collection")
public class Collection {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "collection_id")
    private Long collectionId;
    @Column(name = "name")
    private String collectionName;
    @OneToOne
    @JsonProperty("firstItem")
    @JoinColumns({
            @JoinColumn(name="collection_id", referencedColumnName="collection_id",
                    insertable = false, updatable = false),
            @JoinColumn(name="first_item", referencedColumnName="item_id",
                    insertable = false, updatable = false)
    })
    private Item firstItem;
    @Column(name = "first_item")
    @JsonIgnore
    private Long firstItemId;

    public void setFirstItem(Item firstItem) {
        // TODO: Propagate any changes to firstItem too?
        if (firstItem == null || firstItem.getId() == null || firstItem.getId().getItemId() == null)
            this.firstItemId = null;
        else
            this.firstItemId = firstItem.getId().getItemId();
    }

}
