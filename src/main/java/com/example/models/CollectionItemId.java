package com.example.models;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by flesk on 07.12.16.
 */
@Data
@Embeddable
public class CollectionItemId implements Serializable {
    @Column(name = "collection_id")
    Long collectionId;
    @Column(name = "item_id")
    Long itemId;

    public CollectionItemId() {
    }

    public CollectionItemId(Long collectionId, Long itemId) {
        this.collectionId = collectionId;
        this.itemId = itemId;
    }
}
