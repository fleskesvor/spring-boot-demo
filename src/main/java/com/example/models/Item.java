package com.example.models;

import com.example.models.types.Kajigger;
import com.example.models.types.Thingy;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import javax.persistence.*;

/**
 * Created by flesk on 07.12.16.
 */
@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
/*@Table(name = "item",
        uniqueConstraints = @UniqueConstraint(columnNames = {
                "collection_id",
                "item_id"
        })
)*/
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "itemType"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Thingy.class, name = "Thingy"),
        @JsonSubTypes.Type(value = Kajigger.class, name = "Kajigger")
})
public class Item {
    @EmbeddedId
    CollectionItemId id;
    @Column(name = "item_type")
    String itemType;

    /*
    @OneToOne
    @JoinColumns({
            @JoinColumn(name="collection_id", referencedColumnName="collection_id"),
            @JoinColumn(name="item_id", referencedColumnName="first_item")
    })
    private Collection collection;
    */

    public void setId(Long collectionId, Long itemId) {
        if (id == null)
            id = new CollectionItemId();
        id.setCollectionId(collectionId);
        id.setItemId(itemId);
    }
}