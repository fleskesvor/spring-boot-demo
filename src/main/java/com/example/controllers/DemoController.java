package com.example.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by eivind on 2016-12-09.
 */
@RestController
@RequestMapping("demo")
public class DemoController {

    @RequestMapping(produces = "application/json")
    public HashMap<String, String> getJsonResponse() {
        System.out.println("Requesting JSON response");
        return new HashMap<String, String>() {{
            put("message", "Test JSON response");
        }};
    }

    @RequestMapping(produces = "text/plain")
    public String getTextResponse() {
        System.out.println("Requesting text response");
        return "Test text response";
    }
}
