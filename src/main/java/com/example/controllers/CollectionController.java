package com.example.controllers;

import com.example.models.Collection;
import com.example.models.Item;
import com.example.services.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by flesk on 07.12.16.
 */
@RestController
@RequestMapping("collections")
public class CollectionController {

    @Autowired
    CollectionService collectionService;

    @RequestMapping(produces = "application/json")
    public List<Collection> getCollections() {
        return collectionService.getCollections();
    }

    @RequestMapping(path = "{collectionId}", method = RequestMethod.GET, produces = "application/json")
    public Collection getCollection(@PathVariable("collectionId") Long collectionId) {
        return collectionService.getCollection(collectionId);
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public Collection updateCollection(@RequestBody Collection collection) {
        return collectionService.saveCollection(collection);
    }

    @RequestMapping(path = "{collectionId}/items", method = RequestMethod.GET, produces = "application/json")
    public List<Item> getItems(@PathVariable("collectionId") Long collectionId) {
        return collectionService.getItems(collectionId);
    }

    @RequestMapping(path = "{collectionId}/items/{itemId}", method = RequestMethod.GET, produces = "application/json")
    public Item getItem(@PathVariable("collectionId") Long collectionId, @PathVariable("itemId") Long itemId) {
        return collectionService.getItem(collectionId, itemId);
    }

    @RequestMapping(path = "{collectionId}/items", method = RequestMethod.POST, produces = "application/json")
    public Item addItem(@PathVariable("collectionId") Long collectionId, @RequestBody Item item) {
        return collectionService.saveItem(collectionId, item);
    }
}
