package com.example.exceptions;

import java.util.NoSuchElementException;

/**
 * Created by eivind on 2016-12-13.
 */
public class EntityNotFoundException extends NoSuchElementException {
    public EntityNotFoundException(String message) {
        super(message);
    }
}
