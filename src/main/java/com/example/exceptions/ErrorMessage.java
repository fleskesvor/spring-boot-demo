package com.example.exceptions;

/**
 * Created by eivind on 2016-12-13.
 */

import lombok.Data;

@Data
public class ErrorMessage {
    String error;

    public ErrorMessage(String error) {
        this.error = error;
    }
}
