package com.example.exceptions;

/**
 * Created by eivind on 2016-12-13.
 */
public class EntityNotSavedException extends IllegalArgumentException {
    public EntityNotSavedException(String message) {
        super(message);
    }
}
