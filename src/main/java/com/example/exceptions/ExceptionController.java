package com.example.exceptions;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by eivind on 2016-12-13.
 */
@ControllerAdvice
public class ExceptionController {

    // TODO: Handle severity depending on exception
    static final Logger logger = LogManager.getLogger(ExceptionController.class.getName());

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorMessage handleException(EntityNotFoundException exception) {
        logger.info(exception.getMessage());
        return new ErrorMessage(exception.getMessage());
    }

    @ExceptionHandler(EntityNotSavedException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorMessage handleException(EntityNotSavedException exception) {
        logger.info(exception.getMessage());
        return new ErrorMessage(exception.getMessage());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorMessage handleException(DataIntegrityViolationException exception) {
        // TODO: Consider pros and cons of this approach vs. changing service to handle it
        return new ErrorMessage(exception.getMessage());
    }

}
