package com.example.services;

import com.example.models.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by eivind on 2016-12-13.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CollectionServiceTest {

    // TODO: Needs improvement (and does this serve a purpose vs. in-memory database?)

    @Autowired
    CollectionService collectionService;

    @Test
    public void testGetCollection() {
        Long collectionId = 1L;
        Collection collection = collectionService.getCollection(collectionId);
        assertNotNull(collection);
        assertEquals(collectionId, collection.getCollectionId());
        System.out.println(collection);
    }

    @Test
    public void testGetCollectionList() {
        List<Collection> collectionList = collectionService.getCollections();
        assertNotNull(collectionList);
        assertTrue(collectionList.size() > 0);
        for (Collection collection : collectionList)
            System.out.println(collection);
    }
}
