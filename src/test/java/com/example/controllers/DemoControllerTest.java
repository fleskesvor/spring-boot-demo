package com.example.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by flesk on 11.12.16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoControllerTest {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    private String url = null;

    @Before
    public void setUp() {
        url = "http://localhost:" + port + "/demo/";
    }

    @Test
    public void testGetString() {
        String response = restTemplate.getForObject(url, String.class);
        assertNotNull(response);
        assertTrue(response.length() > 0);
        System.out.println(response);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetMap() {
        HashMap<String, String> response = restTemplate.getForObject(url, HashMap.class);
        assertNotNull(response);
        assertTrue(response.containsKey("message"));
        System.out.println(response);
    }
}

