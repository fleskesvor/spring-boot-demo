package com.example.controllers;

import com.example.models.Collection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by flesk on 08.12.16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CollectionControllerTest {

    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    private String url = null;

    @Before
    public void setUp() {
        url = "http://localhost:" + port + "/collections/";
    }

    @Test
    public void testGetCollections() {
        // String contains serialized firstItem -- Collection objects don't
        System.out.println(restTemplate.getForEntity(url, String.class));

        ResponseEntity<Collection[]> response = restTemplate.getForEntity(url, Collection[].class);
        Collection[] collectionList = response.getBody();
        assertNotNull(collectionList);
        assertTrue(collectionList.length > 0);

        for (Collection collection : collectionList)
            System.out.println(collection);
    }

    @Test
    public void testGetCollection() {
        Collection collection = restTemplate.getForObject(url + "1", Collection.class);
        System.out.println(collection);
    }

}
