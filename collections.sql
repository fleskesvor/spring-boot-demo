CREATE DATABASE collections;

CREATE USER 'manager'@'localhost' IDENTIFIED BY 'password123';
GRANT ALL PRIVILEGES ON collections.* to 'manager'@'localhost';

USE collections;

CREATE TABLE collection (
    collection_id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    first_item INT,
    PRIMARY KEY (collection_id)
);

CREATE TABLE item (
    collection_id INT NOT NULL,
    item_id INT NOT NULL,
    item_type VARCHAR(50),
    KEY (collection_id, item_id),
    FOREIGN KEY (collection_id)
    REFERENCES collection(collection_id)
);

CREATE TABLE thingy (
    collection_id INT NOT NULL,
    item_id INT NOT NULL,
    name VARCHAR(50),
    next_item INT,
    FOREIGN KEY (collection_id, item_id)
    REFERENCES item(collection_id, item_id)
);

CREATE TABLE kajigger (
    collection_id INT NOT NULL,
    item_id INT NOT NULL,
    name VARCHAR(50),
    description VARCHAR(100),
    FOREIGN KEY (collection_id, item_id)
    REFERENCES item(collection_id, item_id)
);

INSERT INTO collection (name, first_item) VALUES ('First Collection', 1);
INSERT INTO collection (name, first_item) VALUES ('Second Collection', 1);

INSERT INTO item (collection_id, item_id, item_type) VALUES (1, 1, 'Thingy');
INSERT INTO item (collection_id, item_id, item_type) VALUES (1, 2, 'Kajigger');
INSERT INTO item (collection_id, item_id, item_type) VALUES (2, 1, 'Kajigger');

INSERT INTO thingy (collection_id, item_id, name) VALUES (1, 1, 'First Thingy');
INSERT INTO kajigger VALUES (1, 2, 'First Kajigger', 'This is our first Kajigger');
INSERT INTO kajigger VALUES (2, 1, 'Second Kajigger', 'This is our second Kajigger');
