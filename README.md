Spring Boot Demo
================

Checking out [Spring Boot](http://projects.spring.io/spring-boot/) with REST and CRUD.

Run with `mvn spring-boot:run`